Parametrization Cookbook
========================

This is a module to handle bijective parametrizations for using Machine Learning
methods in Statistical Inference.

Common transformation of constrained problems to unconstrained problems are
implemented. See [the documentation][doc] and [examples in the
documentation][doc].

To install:

```bash
pip install --upgrade parametrization-cookbook
```

[The documentation is available here.][doc]

[doc]: https://jbleger.gitlab.io/parametrization-cookbook/index.html
