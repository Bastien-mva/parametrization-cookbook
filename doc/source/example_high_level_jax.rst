.. math::


   \newcommand\p[1]{{\left(#1\right)}}
   \newcommand\abs[1]{{\left|#1\right|}}
   \newcommand\brackets[1]{{\left[#1\right]}}
   \newcommand\braces[1]{{\left\{#1\right\}}}
   \newcommand\ouv[2]{\p{#1,#2}}
   \newcommand\repar[2]{r_{#1\to#2}}
   \newcommand\reparp[3]{r_{#1;#2\to#3}}
   \newcommand\reels{{\mathbb R}}
   \newcommand\reelsp{{\mathbb R_+^*}}
   \newcommand\natup{\mathbb N^*}
   \newcommand\simplex[1]{\mathcal S_{#1}}
   \newcommand\osimplex[1]{\mathring{\mathcal S}_{#1}}
   \newcommand\concat{\operatorname{concat}}
   \newcommand\cumsum{\operatorname{cumsum}}
   \newcommand\cumprod{\operatorname{cumprod}}
   \newcommand\flip{\operatorname{flip}}
   \newcommand\logit{\operatorname{logit}}
   \newcommand\expit{\operatorname{expit}}
   \newcommand\logupexp{\operatorname{log1pexp}}
   \newcommand\logexpmu{\operatorname{logexpm1}}
   \newcommand\range{\operatorname{range}}
   \newcommand\Logistic{\operatorname{Logistic}}
   \newcommand\erf{\operatorname{erf}}
   \newcommand\erfinv{\operatorname{erfinv}}
   \newcommand\esp{\mathbb{E}}
   \newcommand\sphere[1]{\mathbf{S}_{#1}}
   \newcommand\msphere[1]{\tilde{\mathbf{S}}_{#1}}
   \newcommand\hsphere[1]{\mathbf{HS}_{#1}}
   \newcommand\ball[1]{\mathbf{B}_{#1}}
   \newcommand\oball[1]{\mathring{\mathbf{B}}_{#1}}
   \newcommand\appsim{\underset{\text{approx}}\sim}
   \newcommand\owedge{\bigtriangleup}

Inference using High-Level interface with JAX
=============================================

Introduction
------------

With :math:`\mu\in\reels^p`, :math:`\Sigma` a symmetric definite
positive matrix of size :math:`p`, and :math:`\nu\in\reelsp`, the
multivariate Student distribution with parameters
:math:`(\mu,\Sigma,\nu)` is defined by the following probability
distribution function:

.. math::


      \begin{array}{rcl}
       \mathbb R^p & \longrightarrow & \mathbb R_+ \\
       x & \longmapsto &
       \frac{\Gamma\left(\frac{\nu+p}2\right)}{\Gamma\left(\frac\nu2\right)\nu^{\frac p2}\pi^{\frac p2}\left|\Sigma\right|^{\frac12}}{\left(1+\frac1\nu(x-\mu)^T\Sigma^{-1}(x-\mu)\right)}^{-\frac{\nu+p}2}
      \end{array}

With :math:`Y\sim\mathcal N(0,\Sigma)`, :math:`Z\sim\chi^2_\nu`, then
:math:`X = \mu+\frac{Y}{\sqrt{Z/\nu}}` follows a multivariate student
distribution with parameters :math:`(\mu,\Sigma,\nu)`.

The idea of this example is to introduce the inference with Maximum
Likelihood Estimator (MLE) of the parameters, handling the constraints
by parametrization with high level interface of the package, using
automatic differentiation to compute derivatives.

We introduce :math:`\theta\in\reels^k` the mapping of
:math:`(\mu, \Sigma, \nu)` by our bijective parametrization. Using
invariance property of the MLE, the mapping of MLE of :math:`\theta` is
equivalent to the MLE of :math:`(\mu, \Sigma, \nu)`.

In a second time, when a MLE is obtained, with sufficient regularity
conditions (not detailed here), using asymptotic properties of MLE and
Slutsky’s lemma we have:

.. math::


       \widehat I_{n,\widehat\theta}^{-\frac12}\p{\widehat\theta-\theta_0}
       \underset{n\to+\infty}\longrightarrow \mathcal N\p{0, I}

where:

-  the estimated Fisher information matrix
   :math:`\widehat I_{n,\widehat\theta} = - {\left.\frac{\operatorname{d}^2\ell\p{\theta, \ldots}}{\operatorname{d}\theta^2}\right|}_{\theta=\widehat\theta}`.
-  :math:`I` the identity matrix.
-  :math:`\ell` is the log-likelihood of the whole sample.

Now we can move this result to our original parameter space:

.. math::


   \frac{\widehat\nu-\nu_0}{\sqrt{\delta_\nu^TI_{n,\widehat\theta}^{-1}\delta_\nu}}\underset{n\to+\infty}\longrightarrow\mathcal N(0,1)

with:

-  :math:`\delta_\nu = {\left.\frac{\operatorname{d}\nu}{\operatorname{d}\theta}\right|}_{\theta=\widehat\theta}`

Therefore we can obtain asymptotic confidence interval:

.. math::


   \mathbb P\p{
   \nu_0\in
   \left[\widehat\nu \pm
      u_{1-\alpha/2}\sqrt{\delta_\nu^TI_{n,\widehat\theta}^{-1}\delta_\nu}\right]
   } \underset{n\to+\infty}\longrightarrow 1-\alpha

Note that :math:`\delta_\nu` and :math:`I_{n,\widehat\theta}` will be
computed with automatic differentiation.

The same method is applicable with any parameter or function of
parameter, *e.g.* we can have a confidence interval on
:math:`\abs{\Sigma}`:

.. math::


   \mathbb P\p{
   \abs{\Sigma_0}\in
   \left[\abs{\widehat\Sigma} \pm
      u_{1-\alpha/2}\sqrt{\delta_{\abs\Sigma}^TI_{n,\widehat\theta}^{-1}\delta_{\abs\Sigma}}\right]
   } \underset{n\to+\infty}\longrightarrow 1-\alpha

with:

-  :math:`\delta_{\abs\Sigma} = {\left.\frac{\operatorname{d}\abs{\Sigma}}{\operatorname{d}\theta}\right|}_{\theta=\widehat\theta}`

Simulating the data
-------------------

First we generate simulated data to illustrate the method.

.. container:: cell

   .. code:: python

      import numpy as np
      import scipy.stats

      n = 1000
      mu = np.arange(3)
      Sigma = np.array([[2, 1, 1], [1, 2, 1.5], [1, 1.5, 2]])
      df = 7

      # we use a seeded random state only for reproducibility
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(0)))
      X = scipy.stats.multivariate_t(loc=mu, shape=Sigma, df=df).rvs(
          size=n, random_state=random_state
      )

      # convert to JAX array
      import jax.numpy as jnp
      X = jnp.array(X)

Definition of the parametrization
---------------------------------

Our parameter space is the Cartesian product of :math:`\mathbb R^3` (for
:math:`\mu`), the space of symmetric definite positive matrices of size
3 (for :math:`\Sigma`) and :math:`\mathbb R_+^*` (for the degree of
freedom :math:`\nu`). To handle these constraints we define a
parametrization between this space and :math:`\mathbb R^k` (the value of
:math:`k` will be automatically computed).

.. container:: cell

   .. code:: python

      import parametrization_cookbook.jax as pc

      parametrization = pc.NamedTuple(
           mu=pc.Real(shape=3),
           Sigma=pc.MatrixSymPosDef(dim=3),
           df=pc.RealPositive()
      )

We can retrieve the value of :math:`k` with ``parametrization.size``.

.. container:: cell

   .. code:: python

      parametrization.size

   .. container:: cell-output cell-output-display

      ::

         10

Definition of the log-likelihood and gradients
----------------------------------------------

First we define the likelihood depending on our original parameters:

.. container:: cell

   .. code:: python

      def original_loglikelihood(mu, Sigma, df, X):
          n, p = X.shape

          eigvals, eigvect = jnp.linalg.eigh(Sigma)
          U = eigvect * (eigvals**-0.5)
          logdet = jnp.log(eigvals).sum()

          return (
              jax.scipy.special.gammaln((df + p) / 2)
              - jax.scipy.special.gammaln(df / 2)
              - p / 2 * jnp.log(df * jnp.pi)
              - 1 / 2 * logdet
              - (
                  (df + p)
                  / 2
                  * jnp.log1p((((X - mu) @ U) ** 2).sum(axis=1) / df)
              )
          ).sum()

And we define the log-likelihood of our parametrized model:

.. container:: cell

   .. code:: python

      import jax

      @jax.jit
      def loglikelihood(theta, X):
          my_params = parametrization.reals1d_to_params(theta)
          return original_loglikelihood(my_params.mu, my_params.Sigma, my_params.df, X)

This function was JIT-compiled, as this function is run many-times, this
is very interesting to reduce computation time.

We now define the gradient and hessian functions (with JIT-compilation):

.. container:: cell

   .. code:: python

      grad_loglikelihood = jax.jit(jax.grad(loglikelihood))
      hessian_loglikelihood = jax.jit(jax.jacfwd(jax.jacrev(loglikelihood)))

Optimization
------------

We can use any optimization algorithm. We choose here a gradient descend
(with step conditioning by the highest eigenvalue of the hessian)
followed by the Newton-Raphson method. The gradient method is choosen
for its robustness, and the second for its quick convergence starting
from a initial point close to the optimum. This only given for
illustration purpose, in a real application case, using optimization
algorithm developed with JAX in the Python module ``jaxopt`` (\ `Blondel
et al. 2021 <#ref-jaxopt>`__\ ) can be a better choice.

We choose here to initialize randomly :math:`\theta`. We can also build
a plausible value of :math:`\theta` with
``parametrization.params_to_reals1d``.

.. container:: cell

   .. code:: python

      import itertools

      # we use a seeded random state only for reproducibility
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(1)))
      theta = random_state.normal(size=parametrization.size)

      current_likeli = loglikelihood(theta, X)
      print(f"current_likli = {current_likeli}")

      for it_grad in itertools.count():
          g = grad_loglikelihood(theta, X)
          H = hessian_loglikelihood(theta, X)
          sdp_eigenvalues = -jnp.linalg.eigh(H)[0]
          lr = 1/sdp_eigenvalues.max()
          theta += lr*g
          current_likeli, old_likeli = loglikelihood(theta, X), current_likeli
          if current_likeli-old_likeli<1e-2:
              break
      print(f'it_grad: {it_grad}, current_likli: {current_likeli}')

      for it_nr in itertools.count():
          g = grad_loglikelihood(theta, X)
          H = hessian_loglikelihood(theta, X)
          theta += -min(1,.1*2**it_nr)*jnp.linalg.solve(H, g)
          current_likeli, old_likeli = loglikelihood(theta, X), current_likeli
          if it_nr>3 and current_likeli-old_likeli<1e-6:
              break
      print(f'it_nr: {it_nr}, current_likli: {current_likeli}')
      print(f'theta: {theta}')

   .. container:: cell-output cell-output-stdout

      ::

         current_likli = -17553.708984375

   .. container:: cell-output cell-output-stdout

      ::

         it_grad: 481, current_likli: -5160.08642578125
         it_nr: 6, current_likli: -5156.76318359375
         theta: [0.01115316 1.0506299  2.0488813  1.1360171  1.6467183  1.3518243
          1.0945351  1.2938223  1.5352948  7.492975  ]

Using the value
---------------

It is easy to retrieve estimates
:math:`(\widehat\mu,\widehat\Sigma,\widehat\nu)`:

.. container:: cell

   .. code:: python

      my_params = parametrization.reals1d_to_params(theta)
      my_params.mu

   .. container:: cell-output cell-output-display

      ::

         DeviceArray([0.01115316, 1.0506299 , 2.0488813 ], dtype=float32)

.. container:: cell

   .. code:: python

      my_params.Sigma

   .. container:: cell-output cell-output-display

      ::

         DeviceArray([[2.0007489, 1.09474  , 1.0565993],
                      [1.09474  , 2.260526 , 1.720708 ],
                      [1.0565993, 1.720708 , 2.1778986]], dtype=float32)

.. container:: cell

   .. code:: python

      my_params.df

   .. container:: cell-output cell-output-display

      ::

         DeviceArray(7.493532, dtype=float32)

We can see we recover good estimate of the simulated parameters.

Building confidence interval
----------------------------

The first step is to compute the inverse of the estimated Fisher
information matrix :math:`\widehat I_{n,\widehat\theta}`:

.. container:: cell

   .. code:: python

      FIM_inv = jnp.linalg.inv(-H)

And we can compute :math:`\delta_\nu`, then the confidence interval:

.. container:: cell

   .. code:: python

      delta_df = jax.grad(lambda theta: parametrization.reals1d_to_params(theta).df)(theta)
      df_asymptotic_variance = delta_df @ FIM_inv @ delta_df
      df_confidence_interval = (
          parametrization.reals1d_to_params(theta).df
          + jnp.array([-1, 1]) * scipy.stats.norm.ppf(0.975) * jnp.sqrt(df_asymptotic_variance)
      )
      print(df_confidence_interval)

   .. container:: cell-output cell-output-stdout

      ::

         [5.815369 9.171696]

The simulated value was :math:`7`.

For the confidence interval on :math:`\abs\Sigma`, we have:

.. container:: cell

   .. code:: python

      delta_det = jax.grad(
          lambda theta: jnp.linalg.det(parametrization.reals1d_to_params(theta).Sigma)
      )(theta)
      det_asymptotic_variance = delta_det @ FIM_inv @ delta_det
      det_confidence_interval = (
          jnp.linalg.det(parametrization.reals1d_to_params(theta).Sigma)
          + jnp.array([-1, 1]) * scipy.stats.norm.ppf(0.975) * np.sqrt(det_asymptotic_variance)
      )
      print(det_confidence_interval)

   .. container:: cell-output cell-output-stdout

      ::

         [2.1325927 3.4136271]

The simulated value was :math:`2.5`.

.. container:: references csl-bib-body hanging-indent
   :name: refs

   .. container:: csl-entry
      :name: ref-jaxopt

      Blondel, Mathieu, Quentin Berthet, Marco Cuturi, Roy Frostig,
      Stephan Hoyer, Felipe Llinares-López, Fabian Pedregosa, and
      Jean-Philippe Vert. 2021. “Efficient and Modular Implicit
      Differentiation.” *arXiv Preprint arXiv:2105.15183*.
