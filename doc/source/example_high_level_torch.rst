.. math::


   \newcommand\p[1]{{\left(#1\right)}}
   \newcommand\abs[1]{{\left|#1\right|}}
   \newcommand\brackets[1]{{\left[#1\right]}}
   \newcommand\braces[1]{{\left\{#1\right\}}}
   \newcommand\ouv[2]{\p{#1,#2}}
   \newcommand\repar[2]{r_{#1\to#2}}
   \newcommand\reparp[3]{r_{#1;#2\to#3}}
   \newcommand\reels{{\mathbb R}}
   \newcommand\reelsp{{\mathbb R_+^*}}
   \newcommand\natup{\mathbb N^*}
   \newcommand\simplex[1]{\mathcal S_{#1}}
   \newcommand\osimplex[1]{\mathring{\mathcal S}_{#1}}
   \newcommand\concat{\operatorname{concat}}
   \newcommand\cumsum{\operatorname{cumsum}}
   \newcommand\cumprod{\operatorname{cumprod}}
   \newcommand\flip{\operatorname{flip}}
   \newcommand\logit{\operatorname{logit}}
   \newcommand\expit{\operatorname{expit}}
   \newcommand\logupexp{\operatorname{log1pexp}}
   \newcommand\logexpmu{\operatorname{logexpm1}}
   \newcommand\range{\operatorname{range}}
   \newcommand\Logistic{\operatorname{Logistic}}
   \newcommand\erf{\operatorname{erf}}
   \newcommand\erfinv{\operatorname{erfinv}}
   \newcommand\esp{\mathbb{E}}
   \newcommand\sphere[1]{\mathbf{S}_{#1}}
   \newcommand\msphere[1]{\tilde{\mathbf{S}}_{#1}}
   \newcommand\hsphere[1]{\mathbf{HS}_{#1}}
   \newcommand\ball[1]{\mathbf{B}_{#1}}
   \newcommand\oball[1]{\mathring{\mathbf{B}}_{#1}}
   \newcommand\appsim{\underset{\text{approx}}\sim}
   \newcommand\owedge{\bigtriangleup}

Inference using High-Level interface with PyTorch
=================================================

Introduction
------------

With :math:`\mu\in\reels^p`, :math:`\Sigma` a symmetric definite
positive matrix of size :math:`p`, and :math:`\nu\in\reelsp`, the
multivariate Student distribution with parameters
:math:`(\mu,\Sigma,\nu)` is defined by the following probability
distribution function:

.. math::


      \begin{array}{rcl}
       \mathbb R^p & \longrightarrow & \mathbb R_+ \\
       x & \longmapsto &
       \frac{\Gamma\left(\frac{\nu+p}2\right)}{\Gamma\left(\frac\nu2\right)\nu^{\frac p2}\pi^{\frac p2}\left|\Sigma\right|^{\frac12}}{\left(1+\frac1\nu(x-\mu)^T\Sigma^{-1}(x-\mu)\right)}^{-\frac{\nu+p}2}
      \end{array}

With :math:`Y\sim\mathcal N(0,\Sigma)`, :math:`Z\sim\chi^2_\nu`, then
:math:`X = \mu+\frac{Y}{\sqrt{Z/\nu}}` follows a multivariate student
distribution with parameters :math:`(\mu,\Sigma,\nu)`.

The idea of this example is to introduce the inference with Maximum
Likelihood Estimator (MLE) of the parameters, handling the constraints
by parametrization with high level interface of the package, using
automatic differentiation to compute derivatives.

We introduce :math:`\theta\in\reels^k` the mapping of
:math:`(\mu, \Sigma, \nu)` by our bijective parametrization. Using
invariance property of the MLE, the mapping of MLE of :math:`\theta` is
equivalent to the MLE of :math:`(\mu, \Sigma, \nu)`.

In a second time, when a MLE is obtained, with sufficient regularity
conditions (not detailed here), using asymptotic properties of MLE and
Slutsky’s lemma we have:

.. math::


       \widehat I_{n,\widehat\theta}^{-\frac12}\p{\widehat\theta-\theta_0}
       \underset{n\to+\infty}\longrightarrow \mathcal N\p{0, I}

where:

-  the estimated Fisher information matrix
   :math:`\widehat I_{n,\widehat\theta} = - {\left.\frac{\operatorname{d}^2\ell\p{\theta, \ldots}}{\operatorname{d}\theta^2}\right|}_{\theta=\widehat\theta}`.
-  :math:`I` the identity matrix.
-  :math:`\ell` is the log-likelihood of the whole sample.

Now we can move this result to our original parameter space:

.. math::


   \frac{\widehat\nu-\nu_0}{\sqrt{\delta_\nu^TI_{n,\widehat\theta}^{-1}\delta_\nu}}\underset{n\to+\infty}\longrightarrow\mathcal N(0,1)

with:

-  :math:`\delta_\nu = {\left.\frac{\operatorname{d}\nu}{\operatorname{d}\theta}\right|}_{\theta=\widehat\theta}`

Therefore we can obtain asymptotic confidence interval:

.. math::


   \mathbb P\p{
   \nu_0\in
   \left[\widehat\nu \pm
      u_{1-\alpha/2}\sqrt{\delta_\nu^TI_{n,\widehat\theta}^{-1}\delta_\nu}\right]
   } \underset{n\to+\infty}\longrightarrow 1-\alpha

Note that :math:`\delta_\nu` and :math:`I_{n,\widehat\theta}` will be
computed with automatic differentiation.

The same method is applicable with any parameter or function of
parameter, *e.g.* we can have a confidence interval on
:math:`\abs{\Sigma}`:

.. math::


   \mathbb P\p{
   \abs{\Sigma_0}\in
   \left[\abs{\widehat\Sigma} \pm
      u_{1-\alpha/2}\sqrt{\delta_{\abs\Sigma}^TI_{n,\widehat\theta}^{-1}\delta_{\abs\Sigma}}\right]
   } \underset{n\to+\infty}\longrightarrow 1-\alpha

with:

-  :math:`\delta_{\abs\Sigma} = {\left.\frac{\operatorname{d}\abs{\Sigma}}{\operatorname{d}\theta}\right|}_{\theta=\widehat\theta}`

Simulating the data
-------------------

First we generate simulated data to illustrate the method.

.. container:: cell

   .. code:: python

      import numpy as np
      import scipy.stats

      n = 1000
      mu = np.arange(3)
      Sigma = np.array([[2, 1, 1], [1, 2, 1.5], [1, 1.5, 2]])
      df = 7

      # we use a seeded random state only for reproducibility
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(0)))
      X = scipy.stats.multivariate_t(loc=mu, shape=Sigma, df=df).rvs(
          size=n, random_state=random_state
      )

      # convert to torch
      import torch
      X = torch.tensor(X, dtype=torch.float32)

Definition of the parametrization
---------------------------------

Our parameter space is the Cartesian product of :math:`\mathbb R^3` (for
:math:`\mu`), the space of symmetric definite positive matrices of size
3 (for :math:`\Sigma`) and :math:`\mathbb R_+^*` (for the degree of
freedom :math:`\nu`). To handle these constraints we define a
parametrization between this space and :math:`\mathbb R^k` (the value of
:math:`k` will be automatically computed).

.. container:: cell

   .. code:: python

      import parametrization_cookbook.torch as pc

      parametrization = pc.NamedTuple(
           mu=pc.Real(shape=3),
           Sigma=pc.MatrixSymPosDef(dim=3),
           df=pc.RealPositive()
      )

We can retrieve the value of :math:`k` with ``parametrization.size``.

.. container:: cell

   .. code:: python

      parametrization.size

   .. container:: cell-output cell-output-display

      ::

         10

Definition of the log-likelihood and gradients
----------------------------------------------

First we define the likelihood depending on our original parameters:

.. container:: cell

   .. code:: python

      def original_loglikelihood(mu, Sigma, df, X):
          n, p = X.shape

          eigvals, eigvect = torch.linalg.eigh(Sigma)
          U = eigvect * (eigvals**-0.5)
          logdet = torch.log(eigvals).sum()

          return (
              torch.special.gammaln((df + p) / 2)
              - torch.special.gammaln(df / 2)
              - p / 2 * torch.log(df * torch.pi)
              - 1 / 2 * logdet
              - (
                  (df + p)
                  / 2
                  * torch.log1p((((X - mu) @ U) ** 2).sum(axis=1) / df)
              )
          ).sum()

And we define the log-likelihood of our parametrized model:

.. container:: cell

   .. code:: python

      def loglikelihood(theta, X):
          my_params = parametrization.reals1d_to_params(theta)
          return original_loglikelihood(my_params.mu, my_params.Sigma, my_params.df, X)

Optimization
------------

We can use any optimization algorithm. We choose here a ADAM gradient.

We choose here to initialize randomly :math:`\theta`. We can also build
a plausible value of :math:`\theta` with
``parametrization.params_to_reals1d``.

.. container:: cell

   .. code:: python

      import itertools

      # we use a seeded random state only for reproducibility
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(1)))
      theta = random_state.normal(size=parametrization.size)
      theta = torch.tensor(theta, dtype=torch.float32, requires_grad=True)

      print(f"log-likelihood before: {loglikelihood(theta, X)}")
      optimizer = torch.optim.Adam([theta], lr=1/n)
      last_losses = []
      for it in itertools.count():
          optimizer.zero_grad()
          loss = -loglikelihood(theta, X)
          new_loss = loss.detach()
          last_losses.append(new_loss)
          if len(last_losses)>5000:
              last_losses.pop(0)
              if last_losses[0]-last_losses[-1]<0:
                  break
          loss.backward()
          optimizer.step()
      print(f"it: {it}")
      print(f"log-likelihood after: {loglikelihood(theta, X)}")
      print(f"theta: {theta}")

   .. container:: cell-output cell-output-stdout

      ::

         log-likelihood before: -17554.00390625

   .. container:: cell-output cell-output-stdout

      ::

         it: 23814
         log-likelihood after: -5156.763671875
         theta: tensor([0.0112, 1.0506, 2.0489, 1.1360, 1.6467, 1.3518, 1.0945, 1.2938, 1.5353,
                 7.4930], requires_grad=True)

Using the value
---------------

It is easy to retrieve estimates
:math:`(\widehat\mu,\widehat\Sigma,\widehat\nu)`:

.. container:: cell

   .. code:: python

      my_params = parametrization.reals1d_to_params(theta)
      my_params.mu

   .. container:: cell-output cell-output-display

      ::

         tensor([0.0112, 1.0506, 2.0489], grad_fn=<AddBackward0>)

.. container:: cell

   .. code:: python

      my_params.Sigma

   .. container:: cell-output cell-output-display

      ::

         tensor([[2.0007, 1.0947, 1.0566],
                 [1.0947, 2.2605, 1.7207],
                 [1.0566, 1.7207, 2.1779]], grad_fn=<MulBackward0>)

.. container:: cell

   .. code:: python

      my_params.df

   .. container:: cell-output cell-output-display

      ::

         tensor(7.4935, grad_fn=<SelectBackward0>)

We can see we recover good estimate of the simulated parameters.

Building confidence interval
----------------------------

The first step is to compute the inverse of the estimated Fisher
information matrix :math:`\widehat I_{n,\widehat\theta}`:

.. container:: cell

   .. code:: python

      FIM = - torch.autograd.functional.hessian(lambda theta: loglikelihood(theta, X), theta)
      FIM_inv = torch.linalg.inv(FIM)

And we can compute :math:`\delta_\nu`, then the confidence interval:

.. container:: cell

   .. code:: python

      theta.grad.zero_()
      est_df = parametrization.reals1d_to_params(theta).df
      est_df.backward()
      delta_df = theta.grad.detach()
      df_asymptotic_variance = delta_df @ FIM_inv @ delta_df
      df_confidence_interval = (
          est_df.detach()
          + torch.tensor([-1, 1]) * scipy.stats.norm.ppf(0.975) * torch.sqrt(df_asymptotic_variance)
      )
      print(df_confidence_interval)

   .. container:: cell-output cell-output-stdout

      ::

         tensor([5.8149, 9.1722])

The simulated value was :math:`7`.

For the confidence interval on :math:`\abs\Sigma`, we have:

.. container:: cell

   .. code:: python

      theta.grad.zero_()
      est_det = torch.linalg.det(parametrization.reals1d_to_params(theta).Sigma)
      est_det.backward()
      delta_det = theta.grad.detach()
      det_asymptotic_variance = delta_det @ FIM_inv @ delta_det
      det_confidence_interval = (
          est_det.detach()
          + torch.tensor([-1, 1]) * scipy.stats.norm.ppf(0.975) * torch.sqrt(det_asymptotic_variance)
      )
      print(det_confidence_interval)

   .. container:: cell-output cell-output-stdout

      ::

         tensor([2.1326, 3.4136])

The simulated value was :math:`2.5`.
