.. math::


   \newcommand\p[1]{{\left(#1\right)}}
   \newcommand\abs[1]{{\left|#1\right|}}
   \newcommand\brackets[1]{{\left[#1\right]}}
   \newcommand\braces[1]{{\left\{#1\right\}}}
   \newcommand\ouv[2]{\p{#1,#2}}
   \newcommand\repar[2]{r_{#1\to#2}}
   \newcommand\reparp[3]{r_{#1;#2\to#3}}
   \newcommand\reels{{\mathbb R}}
   \newcommand\reelsp{{\mathbb R_+^*}}
   \newcommand\natup{\mathbb N^*}
   \newcommand\simplex[1]{\mathcal S_{#1}}
   \newcommand\osimplex[1]{\mathring{\mathcal S}_{#1}}
   \newcommand\concat{\operatorname{concat}}
   \newcommand\cumsum{\operatorname{cumsum}}
   \newcommand\cumprod{\operatorname{cumprod}}
   \newcommand\flip{\operatorname{flip}}
   \newcommand\logit{\operatorname{logit}}
   \newcommand\expit{\operatorname{expit}}
   \newcommand\logupexp{\operatorname{log1pexp}}
   \newcommand\logexpmu{\operatorname{logexpm1}}
   \newcommand\range{\operatorname{range}}
   \newcommand\Logistic{\operatorname{Logistic}}
   \newcommand\erf{\operatorname{erf}}
   \newcommand\erfinv{\operatorname{erfinv}}
   \newcommand\esp{\mathbb{E}}
   \newcommand\sphere[1]{\mathbf{S}_{#1}}
   \newcommand\msphere[1]{\tilde{\mathbf{S}}_{#1}}
   \newcommand\hsphere[1]{\mathbf{HS}_{#1}}
   \newcommand\ball[1]{\mathbf{B}_{#1}}
   \newcommand\oball[1]{\mathring{\mathbf{B}}_{#1}}
   \newcommand\appsim{\underset{\text{approx}}\sim}
   \newcommand\owedge{\bigtriangleup}

Inference using Low-Level functions with JAX
============================================

Introduction
------------

With :math:`\mu\in\reels^p` and :math:`\beta\in\reelsp`, the Gumbel
distribution is definied with the probability distribution function:

.. math::


      \begin{array}{rcl}
       \reels & \longrightarrow & \mathbb R_+ \\
       x & \longmapsto &
       \frac{\exp\p{-\exp\p{-\frac{x-\mu}\beta}}\exp\p{-\frac{x-\mu}\beta}}\beta
      \end{array}

The idea of this example is to introduce the inference with Maximum
Likelihood Estimator (MLE) of the parameters, handling the constraints
by parametrization with low level interface of the package, using
automatic differentiation to compute derivatives.

We introduce :math:`\theta\in\reels^2` the mapping of
:math:`(\mu, \beta)` by our bijective parametrization. Using invariance
property of the MLE, the mapping of MLE of :math:`\theta` is equivalent
to the MLE of :math:`(\mu, \beta)`.

In a second time, when a MLE is obtained, with sufficiant regularity
conditions (not detailed here), using asymptotic properties of MLE and
Slutsky’s lemma we have:

.. math::


       \widehat I_{n,\widehat\theta}^{-\frac12}\p{\widehat\theta-\theta_0}
       \underset{n\to+\infty}\longrightarrow \mathcal N\p{0, I}

where:

-  the estimated Fisher information matrix
   :math:`\widehat I_{n,\widehat\theta} = - {\left.\frac{\operatorname{d}^2\ell\p{\theta, \ldots}}{\operatorname{d}\theta^2}\right|}_{\theta=\widehat\theta}`.
-  :math:`I` the identity matrix.
-  :math:`\ell` is the log-likelihood of the whole sample.

Now we can move this result in our original parameter space:

.. math::


   \frac{\widehat\beta-\beta_0}{\sqrt{\delta_\beta^TI_{n,\widehat\theta}^{-1}\delta_\beta}}\underset{n\to+\infty}\longrightarrow\mathcal N(0,1)

with:

-  :math:`\delta_\beta = {\left.\frac{\operatorname{d}\beta}{\operatorname{d}\theta}\right|}_{\theta=\widehat\theta}`

Therefore we can obtain asymptotic confidence interval:

.. math::


   \mathbb P\p{
   \beta_0\in
   \left[\widehat\beta \pm
      u_{1-\alpha/2}\sqrt{\delta_\beta^TI_{n,\widehat\theta}^{-1}\delta_\beta}\right]
   } \underset{n\to+\infty}\longrightarrow 1-\alpha

Note that :math:`\delta_\beta` and :math:`I_{n,\widehat\theta}` will be
computed with automatic differentiation.

Simulating the data
-------------------

First we generate simulated data to illustrate the method.

.. container:: cell

   .. code:: python

      import numpy as np
      import scipy.stats

      n = 1000
      mu0 = 5
      beta0 = 2

      # we use a seeded random state only for reproducibilty
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(0)))
      X = scipy.stats.gumbel_r(loc=mu0, scale=beta0).rvs(
          size=n, random_state=random_state
      )

      # convert to JAX array
      import jax.numpy as jnp
      X = jnp.array(X)

Definition of the log-likelihood and gradients
----------------------------------------------

First we define the likelihood depending on our original parameters:

.. container:: cell

   .. code:: python

      def original_loglikelihood(mu, beta, X):
          logz = -(X-mu)/beta
          return (-jnp.exp(logz)+logz-jnp.log(beta)).sum()

And we define the log-likelihood of our parametrized model by using
functions from the ``parametrization_cookbook.functions.jax`` module:

.. container:: cell

   .. code:: python

      import parametrization_cookbook.functions.jax as pcf
      import jax

      @jax.jit
      def loglikelihood(theta, X):
          mu = theta[0]
          beta = pcf.softplus(theta[1])
          return original_loglikelihood(mu, beta, X)

This function was JIT-compiled, as this function is run many-times, this
is very interesting to reduce computation time.

We now define the gradient and hessian functions (with JIT-compilation):

.. container:: cell

   .. code:: python

      grad_loglikelihood = jax.jit(jax.grad(loglikelihood))
      hessian_loglikelihood = jax.jit(jax.jacfwd(jax.jacrev(loglikelihood)))

Optimization
------------

We can use any optimization algorithm. We choose here a gradient descend
(with step conditioning by the highest eigenvalue of the hessian). This
only given for illustration purpose, in a real application case, using
optimization algorithm developed with JAX in the Python module
``jaxopt`` (\ `Blondel et al. 2021 <#ref-jaxopt>`__\ ) can be a better
choice.

We choose here to initialize randomly :math:`\theta`. We can also build
a plausible value of :math:`\theta` with reciprocal functions.

.. container:: cell

   .. code:: python

      import itertools

      # we use a seeded random state only for reproducibility
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(1)))
      theta = random_state.normal(size=2)
      theta = jnp.array(theta)

      current_likeli = loglikelihood(theta, X)
      print(f"Log-likeli: {current_likeli}")

      for it in itertools.count():
          g = grad_loglikelihood(theta, X)
          H = hessian_loglikelihood(theta, X)
          sdp_eigenvalues = -jnp.linalg.eigh(H)[0]
          lr = 1/sdp_eigenvalues.max()
          theta += lr*g
          current_likeli, old_likeli = loglikelihood(theta, X), current_likeli
          if current_likeli-old_likeli<1e-6:
              break
      print(f"it: {it}, Log-likeli: {current_likeli}")
      print(f"theta: {theta}")

   .. container:: cell-output cell-output-stdout

      ::

         Log-likeli: -14577.125

   .. container:: cell-output cell-output-stdout

      ::

         it: 13, Log-likeli: -2253.14013671875
         theta: [4.9774423 1.8211547]

Using the value
---------------

To retrieve the initial parameter, we must use :math:`\widehat\theta`:

.. container:: cell

   .. code:: python

      theta[0] # this is estimated of mu

   .. container:: cell-output cell-output-display

      ::

         DeviceArray(4.9774423, dtype=float32)

.. container:: cell

   .. code:: python

      pcf.softplus(theta[1]) # this is estimated of beta

   .. container:: cell-output cell-output-display

      ::

         DeviceArray(1.9711586, dtype=float32)

We can see we recover good estimate of the simulated parameters.

Building confidence interval
----------------------------

The first step is to compute the inverse of the estimated Fisher
information matrix :math:`\widehat I_{n,\widehat\theta}`:

.. container:: cell

   .. code:: python

      FIM_inv = jnp.linalg.inv(-H)

And we can compute :math:`\delta_\beta`, then the confidence interval:

.. container:: cell

   .. code:: python

      delta_beta = jax.grad(lambda theta: pcf.softplus(theta[1]))(theta)
      beta_asymptotic_variance = delta_beta @ FIM_inv @ delta_beta
      beta_confidence_interval = (
          pcf.softplus(theta[1])
          + jnp.array([-1, 1]) * scipy.stats.norm.ppf(0.975) * jnp.sqrt(beta_asymptotic_variance)
      )
      print(beta_confidence_interval)

   .. container:: cell-output cell-output-stdout

      ::

         [1.8763951 2.065922 ]

The simulated value was :math:`2`.

.. container:: references csl-bib-body hanging-indent
   :name: refs

   .. container:: csl-entry
      :name: ref-jaxopt

      Blondel, Mathieu, Quentin Berthet, Marco Cuturi, Roy Frostig,
      Stephan Hoyer, Felipe Llinares-López, Fabian Pedregosa, and
      Jean-Philippe Vert. 2021. “Efficient and Modular Implicit
      Differentiation.” *arXiv Preprint arXiv:2105.15183*.
