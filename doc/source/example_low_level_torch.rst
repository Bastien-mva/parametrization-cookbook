.. math::


   \newcommand\p[1]{{\left(#1\right)}}
   \newcommand\abs[1]{{\left|#1\right|}}
   \newcommand\brackets[1]{{\left[#1\right]}}
   \newcommand\braces[1]{{\left\{#1\right\}}}
   \newcommand\ouv[2]{\p{#1,#2}}
   \newcommand\repar[2]{r_{#1\to#2}}
   \newcommand\reparp[3]{r_{#1;#2\to#3}}
   \newcommand\reels{{\mathbb R}}
   \newcommand\reelsp{{\mathbb R_+^*}}
   \newcommand\natup{\mathbb N^*}
   \newcommand\simplex[1]{\mathcal S_{#1}}
   \newcommand\osimplex[1]{\mathring{\mathcal S}_{#1}}
   \newcommand\concat{\operatorname{concat}}
   \newcommand\cumsum{\operatorname{cumsum}}
   \newcommand\cumprod{\operatorname{cumprod}}
   \newcommand\flip{\operatorname{flip}}
   \newcommand\logit{\operatorname{logit}}
   \newcommand\expit{\operatorname{expit}}
   \newcommand\logupexp{\operatorname{log1pexp}}
   \newcommand\logexpmu{\operatorname{logexpm1}}
   \newcommand\range{\operatorname{range}}
   \newcommand\Logistic{\operatorname{Logistic}}
   \newcommand\erf{\operatorname{erf}}
   \newcommand\erfinv{\operatorname{erfinv}}
   \newcommand\esp{\mathbb{E}}
   \newcommand\sphere[1]{\mathbf{S}_{#1}}
   \newcommand\msphere[1]{\tilde{\mathbf{S}}_{#1}}
   \newcommand\hsphere[1]{\mathbf{HS}_{#1}}
   \newcommand\ball[1]{\mathbf{B}_{#1}}
   \newcommand\oball[1]{\mathring{\mathbf{B}}_{#1}}
   \newcommand\appsim{\underset{\text{approx}}\sim}
   \newcommand\owedge{\bigtriangleup}

Inference using Low-Level functions with PyTorch
================================================

Introduction
------------

With :math:`\mu\in\reels^p` and :math:`\beta\in\reelsp`, the Gumbel
distribution is definied with the probability distribution function:

.. math::


      \begin{array}{rcl}
       \reels & \longrightarrow & \mathbb R_+ \\
       x & \longmapsto &
       \frac{\exp\p{-\exp\p{-\frac{x-\mu}\beta}}\exp\p{-\frac{x-\mu}\beta}}\beta
      \end{array}

The idea of this example is to introduce the inference with Maximum
Likelihood Estimator (MLE) of the parameters, handling the constraints
by parametrization with low level interface of the package, using
automatic differentiation to compute derivatives.

We introduce :math:`\theta\in\reels^2` the mapping of
:math:`(\mu, \beta)` by our bijective parametrization. Using invariance
property of the MLE, the mapping of MLE of :math:`\theta` is equivalent
to the MLE of :math:`(\mu, \beta)`.

In a second time, when a MLE is obtained, with sufficiant regularity
conditions (not detailed here), using asymptotic properties of MLE and
Slutsky’s lemma we have:

.. math::


       \widehat I_{n,\widehat\theta}^{-\frac12}\p{\widehat\theta-\theta_0}
       \underset{n\to+\infty}\longrightarrow \mathcal N\p{0, I}

where:

-  the estimated Fisher information matrix
   :math:`\widehat I_{n,\widehat\theta} = - {\left.\frac{\operatorname{d}^2\ell\p{\theta, \ldots}}{\operatorname{d}\theta^2}\right|}_{\theta=\widehat\theta}`.
-  :math:`I` the identity matrix.
-  :math:`\ell` is the log-likelihood of the whole sample.

Now we can move this result in our original parameter space:

.. math::


   \frac{\widehat\beta-\beta_0}{\sqrt{\delta_\beta^TI_{n,\widehat\theta}^{-1}\delta_\beta}}\underset{n\to+\infty}\longrightarrow\mathcal N(0,1)

with:

-  :math:`\delta_\beta = {\left.\frac{\operatorname{d}\beta}{\operatorname{d}\theta}\right|}_{\theta=\widehat\theta}`

Therefore we can obtain asymptotic confidence interval:

.. math::


   \mathbb P\p{
   \beta_0\in
   \left[\widehat\beta \pm
      u_{1-\alpha/2}\sqrt{\delta_\beta^TI_{n,\widehat\theta}^{-1}\delta_\beta}\right]
   } \underset{n\to+\infty}\longrightarrow 1-\alpha

Note that :math:`\delta_\beta` and :math:`I_{n,\widehat\theta}` will be
computed with automatic differentiation.

Simulating the data
-------------------

First we generate simulated data to illustrate the method.

.. container:: cell

   .. code:: python

      import numpy as np
      import scipy.stats

      n = 1000
      mu0 = 5
      beta0 = 2

      # we use a seeded random state only for reproducibility
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(0)))
      X = scipy.stats.gumbel_r(loc=mu0, scale=beta0).rvs(
          size=n, random_state=random_state
      )

      # convert to PyTorch tensor
      import torch
      X = torch.tensor(X)

Definition of the log-likelihood and gradients
----------------------------------------------

First we define the likelihood depending on our original parameters:

.. container:: cell

   .. code:: python

      def original_loglikelihood(mu, beta, X):
          logz = -(X-mu)/beta
          return (-torch.exp(logz)+logz-torch.log(beta)).sum()

And we define the log-likelihood of our parametrized model by using
functions from the ``parametrization_cookbook.functions.torch`` module:

.. container:: cell

   .. code:: python

      import parametrization_cookbook.functions.torch as pcf

      def loglikelihood(theta, X):
          mu = theta[0]
          beta = pcf.softplus(theta[1])
          return original_loglikelihood(mu, beta, X)

Optimization
------------

We can use any optimization algorithm. We choose here a ADAM gradient.

We choose here to initialize randomly :math:`\theta`. We can also build
a plausible value of :math:`\theta` with reciprocal functions.

.. container:: cell

   .. code:: python

      import itertools

      # we use a seeded random state only for reproducibility
      random_state = np.random.RandomState(np.random.MT19937(np.random.SeedSequence(1)))
      theta = random_state.normal(size=2)
      theta = torch.tensor(theta, dtype=torch.float32, requires_grad=True)

      current_likeli = loglikelihood(theta, X)
      print(f"log-likelihood before: {loglikelihood(theta, X)}")
      optimizer = torch.optim.Adam([theta], lr=1/n)
      last_losses = []
      for it in itertools.count():
          optimizer.zero_grad()
          loss = -loglikelihood(theta, X)
          new_loss = loss.detach()
          last_losses.append(new_loss)
          if len(last_losses)>5000:
              last_losses.pop(0)
              if last_losses[0]-last_losses[-1]<0:
                  break
          loss.backward()
          optimizer.step()
      print(f"it: {it}")
      print(f"log-likelihood after: {loglikelihood(theta, X)}")
      print(f"theta: {theta}")

   .. container:: cell-output cell-output-stdout

      ::

         log-likelihood before: -14577.124868295035

   .. container:: cell-output cell-output-stdout

      ::

         it: 15350
         log-likelihood after: -2253.1400991199
         theta: tensor([4.9778, 1.8214], requires_grad=True)

Using the value
---------------

To retrieve the initial parameter, we must use :math:`\widehat\theta`:

.. container:: cell

   .. code:: python

      theta[0] # this is estimated of mu

   .. container:: cell-output cell-output-display

      ::

         tensor(4.9778, grad_fn=<SelectBackward0>)

.. container:: cell

   .. code:: python

      pcf.softplus(theta[1]) # this is estimated of beta

   .. container:: cell-output cell-output-display

      ::

         tensor(1.9714, grad_fn=<MulBackward0>)

We can see we recover good estimate of the simulated parameters.

Building confidence interval
----------------------------

The first step is to compute the inverse of the estimated Fisher
information matrix :math:`\widehat I_{n,\widehat\theta}`:

.. container:: cell

   .. code:: python

      FIM = - torch.autograd.functional.hessian(lambda theta: loglikelihood(theta, X), theta)
      FIM_inv = torch.linalg.inv(FIM)

And we can compute :math:`\delta_\beta`, then the confidence interval:

.. container:: cell

   .. code:: python

      theta.grad.zero_()
      beta = pcf.softplus(theta[1])
      beta.backward()
      delta_beta = theta.grad.detach()
      beta_asymptotic_variance = delta_beta @ FIM_inv @ delta_beta
      beta_confidence_interval = (
          beta.detach()
          + torch.tensor([-1, 1]) * scipy.stats.norm.ppf(0.975) * torch.sqrt(beta_asymptotic_variance)
      )
      print(beta_confidence_interval)

   .. container:: cell-output cell-output-stdout

      ::

         tensor([1.8766, 2.0662])

The simulated value was :math:`2`.
