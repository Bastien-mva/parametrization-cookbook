.. Parametrization Cookbook documentation master file, created by
   sphinx-quickstart on Sun Nov 27 19:26:25 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Parametrization Cookbook's documentation!
====================================================

.. toctree::
   :maxdepth: 1
   :caption: API Documentation:

   ./modules_hl.rst
   ./modules_functions.rst

.. toctree::
   :maxdepth: 1
   :caption: Examples:

   ./example_high_level_jax.rst
   ./example_high_level_torch.rst
   ./example_low_level_jax.rst
   ./example_low_level_torch.rst
