API documentation -- High level interface
=========================================

.. toctree::
   :maxdepth: 2

   parametrization_cookbook.numpy
   parametrization_cookbook.jax
   parametrization_cookbook.torch
