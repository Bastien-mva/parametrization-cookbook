parametrization\_cookbook.jax module
====================================

Parametrization of scalars
--------------------------

.. toctree::
   :hidden:
   :maxdepth: 1

   generated_parametrization_cookbook.jax.Real
   generated_parametrization_cookbook.jax.RealPositive
   generated_parametrization_cookbook.jax.RealNegative
   generated_parametrization_cookbook.jax.RealLowerBounded
   generated_parametrization_cookbook.jax.RealUpperBounded
   generated_parametrization_cookbook.jax.RealBounded01
   generated_parametrization_cookbook.jax.RealBounded



All this parametrization is usable for vectors, matrices and nd-arrays with the
same constraints.

* :ref:`Real<parametrization_cookbook.jax.Real>` for :math:`\mathbb R` (or :math:`\mathbb R^{a_0\times\ldots\times a_k}`)
* :ref:`RealPositive<parametrization_cookbook.jax.RealPositive>` for :math:`\mathbb R_+^*` (or :math:`{\mathbb R_+^*}^{a_0\times\ldots\times a_k}`)
* :ref:`RealNegative<parametrization_cookbook.jax.RealNegative>` for :math:`\mathbb R_-^*` (or :math:`{\mathbb R_-^*}^{a_0\times\ldots\times a_k}`)
* :ref:`RealLowerBounded<parametrization_cookbook.jax.RealLowerBounded>` for :math:`(b,+\infty)` (or :math:`{(b,+\infty)}^{a_0\times\ldots\times a_k}`)
* :ref:`RealUpperBounded<parametrization_cookbook.jax.RealUpperBounded>` for :math:`(-\infty,b)` (or :math:`{(-\infty,b)}^{a_0\times\ldots\times a_k}`)
* :ref:`RealBounded01<parametrization_cookbook.jax.RealBounded01>` for :math:`(0,1)` (or :math:`{(0,1)}^{a_0\times\ldots\times a_k}`)
* :ref:`RealBounded<parametrization_cookbook.jax.RealBounded>` for :math:`(b_{lower},b_{upper})` (or :math:`{(b_{lower},b_{upper})}^{a_0\times\ldots\times a_k}`)

Parametrization of vectors
--------------------------

.. toctree::
   :hidden:
   :maxdepth: 1

   generated_parametrization_cookbook.jax.VectorSimplex
   generated_parametrization_cookbook.jax.VectorSphere
   generated_parametrization_cookbook.jax.VectorHalfSphere
   generated_parametrization_cookbook.jax.VectorBall

All this parametrization can be used for nd-arrays, the constraint is on the
last dimension.

* :ref:`VectorSimplex<parametrization_cookbook.jax.VectorSimplex>` for
  :math:`\mathcal{S}_n=\left\{x\in\mathbb {R_+^*}^{n+1}: \sum_{i=0}^n x_i=1\right\}`
  (or :math:`\mathcal{S}_n^{a_0\times\ldots\times a_k})`.
* :ref:`VectorSphere<parametrization_cookbook.jax.VectorSphere>` for
  :math:`\mathbf{S}_n=\left\{x\in\mathbb {R}^{n+1}: \sum_{i=0}^n x_i^2=r^2\right\}`
  (or :math:`\mathbf{S}_n^{a_0\times\ldots\times a_k})`.
* :ref:`VectorHalfSphere<parametrization_cookbook.jax.VectorHalfSphere>` for
  :math:`\mathbf{HS}_n=\left\{x\in\mathbb {R}^{n+1}: x_n>0\wedge\sum_{i=0}^n x_i^2=r^2\right\}`
  (or :math:`\mathbf{HS}_n^{a_0\times\ldots\times a_k})`.
* :ref:`VectorBall<parametrization_cookbook.jax.VectorBall>` for
  :math:`\mathcal{B}_n=\left\{x\in\mathbb {R}^n: \sum_{i=0}^{n-1} x_i^2<r^2\right\}`
  (or :math:`\mathcal{B}_n^{a_0\times\ldots\times a_k})`.

Parametrization of matrices
---------------------------

.. toctree::
   :hidden:
   :maxdepth: 1

   generated_parametrization_cookbook.jax.MatrixDiag
   generated_parametrization_cookbook.jax.MatrixDiagPosDef
   generated_parametrization_cookbook.jax.MatrixSym
   generated_parametrization_cookbook.jax.MatrixSymPosDef
   generated_parametrization_cookbook.jax.MatrixCorrelation

All this parametrization can be used for nd-arrays, the constraint is on the
two last dimensions.

* :ref:`MatrixDiag<parametrization_cookbook.jax.MatrixDiag>` for diagonal
  matrices of :math:`\mathbb R^{n\times n}` (or nd-array where the two last
  dimensions are diagonal for all others fixed indices)
* :ref:`MatrixDiagPosDef<parametrization_cookbook.jax.MatrixDiagPosDef>` for diagonal
  positive definite matrices of :math:`\mathbb R^{n\times n}` (or nd-array where the two last
  dimensions are diagonal positive definite for all others fixed indices)
* :ref:`MatrixSym<parametrization_cookbook.jax.MatrixSym>` for symmetric
  matrices of :math:`\mathbb R^{n\times n}` (or nd-array where the two last
  dimensions are symmetric for all others fixed indices)
* :ref:`MatrixSymPosDef<parametrization_cookbook.jax.MatrixSymPosDef>` for symmetric
  definite positive matrices of :math:`\mathbb R^{n\times n}` (or nd-array where the two last
  dimensions are symmetric definite positive for all others fixed indices)
* :ref:`MatrixCorrelation<parametrization_cookbook.jax.MatrixCorrelation>` for correlation matrices (a.k.a. symmetric
  definite positive matrices with unit diagonal) of :math:`\mathbb R^{n\times n}` (or nd-array where the two last
  dimensions are correlation matrices for all others fixed indices)

Concatenation of parametrizations
---------------------------------

.. toctree::
   :hidden:
   :maxdepth: 1

   generated_parametrization_cookbook.jax.Tuple
   generated_parametrization_cookbook.jax.NamedTuple

It is useful for concatenating elementary parametrization defined before. For a
defined parametrization on :math:`E_0,\ldots,E_k`, you can define a
parametrization on :math:`E_0\times\ldots\times E_k`.

* :ref:`Tuple<parametrization_cookbook.jax.Tuple>` defines a concatenation and
  uses the ``tuple`` structure in python to retrieve elementary variates.
* :ref:`NamedTuple<parametrization_cookbook.jax.NamedTuple>` defines a concatenation
  and uses the ``namedtuple`` structure in python to retrieve elementary
  variates.
