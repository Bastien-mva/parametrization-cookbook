import pytest
import collections


def pytest_addoption(parser):
    parser.addoption(
        "--profile",
        action="store",
        choices=("light", "medium", "stress"),
        default="medium",
        help="",
    )


Config = collections.namedtuple("Config", ("R2P_LOG2N", "P2R_N"))


@pytest.fixture
def profile(request):
    return {
        "light": Config(8, 2**8),
        "medium": Config(14, 2**14),
        "stress": Config(18, 2**18),
    }[request.config.getoption("--profile")]
