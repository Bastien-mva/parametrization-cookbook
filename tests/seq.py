import functools
import operator

import numpy as np

from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence


def reals_points(shape=None, log2n=16, wrap=None):
    rs = RandomState(MT19937(SeedSequence(987654321)))
    if wrap is None:
        wrap = lambda x: x
    if shape is None:
        shape = ()
    dim = functools.reduce(operator.mul, shape, 1)
    for i in range(1, 2**log2n):
        x = rs.uniform(size=dim)
        r = np.log(x) - np.log1p(-x)
        yield wrap(r.reshape(shape))


def reals_points_norm_half(shape=None, log2n=16, wrap=None):
    rs = RandomState(MT19937(SeedSequence(987654321)))
    if wrap is None:
        wrap = lambda x: x
    if shape is None:
        shape = ()
    dim = functools.reduce(operator.mul, shape, 1)
    for i in range(1, 2**log2n):
        r = rs.normal(size=dim) / 2
        yield wrap(r.reshape(shape))
