import pytest
import itertools

import numpy as np
import jax.numpy as jnp
import torch

from parametrization_cookbook import numpy as pcnp
from parametrization_cookbook import jax as pcjax
from parametrization_cookbook import torch as pctorch

from .seq import reals_points, reals_points_norm_half

LOCS = (-10, 0)
SCALES = (0.5, 1.0)
ONE_BOUNDS = (-10, 5)
VECTOR_DIMS = (1, 4)
VECTOR_DIMS_BALL = (2, 4)
MATRIX_DIMS = (1, 2, 4)

MODULES_EPS = (
    (pcnp, 1e-12, np.array),
    (pcjax, 1e-6, jnp.array),
    (pctorch, 1e-6, torch.tensor),
)

SCALAR_CLASSES = (
    [("Real", {"loc": loc, "scale": scale}) for loc in LOCS for scale in SCALES]
    + [("RealPositive", {"scale": scale}) for scale in SCALES]
    + [("RealNegative", {"scale": scale}) for scale in SCALES]
    + [
        ("RealLowerBounded", {"bound": bound, "scale": scale})
        for bound in ONE_BOUNDS
        for scale in SCALES
    ]
    + [
        ("RealUpperBounded", {"bound": bound, "scale": scale})
        for bound in ONE_BOUNDS
        for scale in SCALES
    ]
    + [("RealBounded01", {})]
    + [
        ("RealBounded", {"bound_lower": b1, "bound_upper": b2})
        for b1, b2 in itertools.product(ONE_BOUNDS, ONE_BOUNDS)
        if b1 < b2
    ]
)

VECTOR_CLASSES = (
    [("VectorSimplex", {"dim": dim}) for dim in VECTOR_DIMS]
    + [("VectorSphere", {"dim": dim}) for dim in VECTOR_DIMS]
    + [("VectorHalfSphere", {"dim": dim}) for dim in VECTOR_DIMS]
    + [("VectorBall", {"dim": dim}) for dim in VECTOR_DIMS_BALL]
)

MATRIX_CLASSES = (
    [("MatrixDiag", {"dim": dim}) for dim in MATRIX_DIMS]
    + [
        ("MatrixDiagPosDef", {"dim": dim, "scale": scale})
        for dim in MATRIX_DIMS
        for scale in SCALES
    ]
    + [("MatrixSym", {"dim": dim}) for dim in MATRIX_DIMS]
    + [
        ("MatrixSymPosDef", {"dim": dim, "scale": scale})
        for dim in MATRIX_DIMS
        for scale in SCALES
    ]
    + [("MatrixCorrelation", {"dim": dim}) for dim in MATRIX_DIMS if dim > 1]
)

SHAPES = ((7,), (6, 5), (5, 4, 3), (4, 3, 2, 1))


@pytest.mark.parametrize(
    ("pcclass_eps", "classdesc", "shape"),
    itertools.product(MODULES_EPS, SCALAR_CLASSES, SHAPES),
)
def test_scalar_shape(profile, pcclass_eps, classdesc, shape):
    classname, classargs = classdesc
    pcclass, eps, array = pcclass_eps
    p = getattr(pcclass, classname)(shape=shape, **classargs)
    pscalar = getattr(pcclass, classname)(**classargs)

    r = array(next(reals_points_norm_half(shape=(p.size,), log2n=1)))
    rscalar = r.reshape(shape)
    v = p.reals1d_to_params(r)
    assert v.shape == shape
    for idx in itertools.product(*map(range, shape)):
        assert np.abs(v[idx] - pscalar.reals1d_to_params(rscalar[idx])).max() < eps
    r2 = p.params_to_reals1d(v)
    r2scalar = r2.reshape(shape)
    for idx in itertools.product(*map(range, shape)):
        assert np.abs(r2scalar[idx] - pscalar.params_to_reals1d(v[idx])).max() < eps


@pytest.mark.parametrize(
    ("pcclass_eps", "classdesc", "shape"),
    itertools.product(MODULES_EPS, VECTOR_CLASSES + MATRIX_CLASSES, SHAPES),
)
def test_vector_matrix_shape(profile, pcclass_eps, classdesc, shape):
    classname, classargs = classdesc
    pcclass, eps, array = pcclass_eps
    p = getattr(pcclass, classname)(shape=shape, **classargs)
    pvector = getattr(pcclass, classname)(**classargs)
    vector_shape = pvector.reals1d_to_params(array(np.zeros(pvector.size))).shape

    r = array(next(reals_points_norm_half(shape=(p.size,), log2n=1)))
    rvector = r.reshape(shape + (pvector.size,))
    v = p.reals1d_to_params(r)
    assert v.shape == shape + vector_shape
    for idx in itertools.product(*map(range, shape)):
        assert np.abs(v[idx] - pvector.reals1d_to_params(rvector[idx])).max() < eps
    r2 = p.params_to_reals1d(v)
    r2vector = r2.reshape(shape + (pvector.size,))
    for idx in itertools.product(*map(range, shape)):
        assert np.abs(r2vector[idx] - pvector.params_to_reals1d(v[idx])).max() < eps
