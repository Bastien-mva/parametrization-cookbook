import pytest
import itertools

import numpy as np
import jax.numpy as jnp
import torch

from parametrization_cookbook import numpy as pcnp
from parametrization_cookbook import jax as pcjax
from parametrization_cookbook import torch as pctorch

from .seq import reals_points, reals_points_norm_half

BASE_CLASSES = [
    ("RealPositive", {}),
    ("RealNegative", {}),
    ("RealBounded01", {}),
    ("VectorSimplex", {"dim": 2}),
    ("MatrixSymPosDef", {"dim": 2}),
]

MODULES_EPS = (
    (pcnp, 1e-12, np.array),
    (pcjax, 1e-6, jnp.array),
    (pctorch, 1e-6, torch.tensor),
)


@pytest.mark.parametrize(
    ("pcclass_eps", "classesdesc"),
    itertools.product(
        MODULES_EPS,
        itertools.chain(
            *(
                itertools.combinations(BASE_CLASSES, i)
                for i in range(2, len(BASE_CLASSES) + 1)
            )
        ),
    ),
)
def test_tuple(profile, pcclass_eps, classesdesc):
    pcclass, eps, array = pcclass_eps
    base = tuple(
        getattr(pcclass, classname)(**classargs) for classname, classargs in classesdesc
    )
    p = pcclass.Tuple(*base)
    assert p.size == sum(x.size for x in base)

    r = next(reals_points_norm_half(shape=(p.size,), log2n=1, wrap=array))
    v = p.reals1d_to_params(r)
    assert len(v) == len(base)
    assert len(p.idx_params) == len(base)
    for elem_v, elem_p, idx in zip(v, base, p.idx_params):
        assert (elem_v == elem_p.reals1d_to_params(r[idx])).all()
    r2 = p.params_to_reals1d(v)
    assert r2.shape == r.shape
    for elem_v, elem_p, idx in zip(v, base, p.idx_params):
        assert (elem_p.params_to_reals1d(elem_v) == r2[idx]).all()


@pytest.mark.parametrize(
    ("pcclass_eps", "classesdesc"),
    itertools.product(
        MODULES_EPS,
        itertools.chain(
            *(
                itertools.combinations(BASE_CLASSES, i)
                for i in range(1, len(BASE_CLASSES) + 1)
            )
        ),
    ),
)
def test_namedtuple(profile, pcclass_eps, classesdesc):
    pcclass, eps, array = pcclass_eps
    base = tuple(
        getattr(pcclass, classname)(**classargs) for classname, classargs in classesdesc
    )
    names = tuple(f"sample_name_{i}" for i in range(len(base)))
    basedict = {n: b for n, b in zip(names, base)}
    p = pcclass.NamedTuple(**basedict)
    assert p.size == sum(x.size for x in base)

    r = next(reals_points_norm_half(shape=(p.size,), log2n=1, wrap=array))
    v = p.reals1d_to_params(r)
    assert len(v) == len(base)
    assert len(p.idx_params) == len(base)
    for name in names:
        assert (
            getattr(v, name)
            == basedict[name].reals1d_to_params(r[getattr(p.idx_params, name)])
        ).all()
    r2 = p.params_to_reals1d(v)
    assert r2.shape == r.shape
    for name in names:
        assert (
            basedict[name].params_to_reals1d(getattr(v, name))
            == r2[getattr(p.idx_params, name)]
        ).all()
